﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float timeThreshold;
    private float time;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        transform.Translate(transform.forward * Time.deltaTime * 100f, Space.World);

        if(time >= timeThreshold)
        {
            gameObject.GetComponent<BulletDestroy>().enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponent<BulletDestroy>().enabled = true;
    }
}
