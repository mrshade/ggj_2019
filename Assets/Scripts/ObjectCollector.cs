﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollector : MonoBehaviour
{
    public List<GameObject> objectsToSpawn;
    public float counter = 0f;

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime * 2f;

        if(System.Convert.ToInt16( counter )% 5 == 0)
        {
            GameObject go = GameObject.Instantiate(objectsToSpawn[0]);
            go.transform.position = new Vector3(0f, 10f, -4f);
            counter++;
        }

        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-transform.right * Time.deltaTime * 10f);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(transform.right * Time.deltaTime * 10f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.gameObject.SetActive(false);
    }
}
