﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFire : MonoBehaviour
{
    public float fireTime = .05f;
    public GameObject bullet;

    public int pooledAmount = 40;
    List<GameObject> bullets;

    // Start is called before the first frame update
    void Start()
    {
        bullets = new List<GameObject>();
        for(int i=0;i<pooledAmount;i++)
        {
            GameObject go = (GameObject)Instantiate(bullet);
            go.SetActive(false);
            bullets.Add(go);
        }
        //InvokeRepeating("Fire", fireTime, fireTime);
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.Space))
        {
            Invoke("Fire", fireTime);
        }
    }
    
    void Fire()
    {
        for(int i=0;i<bullets.Count;i++)
        {
            if(!bullets[i].activeInHierarchy)
            {
                bullets[i].transform.rotation = transform.rotation  ;
                bullets[i].transform.position = transform.position;
                
                bullets[i].SetActive(true);
                break;
            }
        }
    }
}
