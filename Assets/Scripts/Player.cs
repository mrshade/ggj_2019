﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Range(0.001f,1f)]public float moveSpeed = 0.25f;
    public float maxSpeed = 1f;
    public float minSpeed = 0.01f;
    public float accelerationIncrement = 10f;
    public float decelerationAmount = 20f;
    public float rotationAmount = 1f;
    public Rect scoreScreen;

    private void OnGUI()
    {
        GUI.TextArea(scoreScreen, GameStatics.count.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        moveSpeed -= Time.deltaTime * decelerationAmount;
        if (moveSpeed <= minSpeed)
        {
            moveSpeed = minSpeed;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //transform.Rotate(transform.up, -rotationAmount);
            transform.Rotate(new Vector3(0f, -rotationAmount, 0f), Space.Self);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            //transform.Rotate(transform.up, rotationAmount);
            transform.Rotate(new Vector3(0f, rotationAmount, 0f), Space.Self);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            //increase the acceleration to the fullest - threshold
            if (moveSpeed < maxSpeed)
            {
                moveSpeed += Time.deltaTime * accelerationIncrement;
            }
        }
        transform.Translate((transform.forward * moveSpeed), Space.World);
    }
}
