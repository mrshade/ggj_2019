﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashHit : MonoBehaviour
{
    public UnityEngine.UI.Slider slider;
    public float acceleration;
    public float deceleration;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (slider.value == 1f)
        {
            Debug.Log("You Won!");
            return;
        }
        slider.value -= Time.deltaTime * deceleration;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            slider.value += Time.deltaTime * acceleration;
        }
        
    }
}
