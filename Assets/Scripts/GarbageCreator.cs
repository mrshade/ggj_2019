﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageCreator : MonoBehaviour
{
    public GameObject[] garbageObjects;
    public GameObject garbagePlacementObject;
    [Range(0f, 1f)] public float radiusOffset = 0.1f;
    [Range(1.0f, 10.0f)] public float garbageInstancesRange = 0.1f;
    public float minDistanceFromCentre = 0.5f;
    public bool HideBaseAfterScattering = false;

    private float radius = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        ScatterInSphericalCoords();
        if(HideBaseAfterScattering)
        {
            gameObject.SetActive(false);
        }
    }

    void ScatterInSphericalCoords()
    {
        float x = 1f;
        float y = 1f;
        float z = 1f;
        Bounds bounds = garbagePlacementObject.GetComponent<Renderer>().bounds;
        radius = Vector3.Distance(bounds.max, bounds.min)*(0.5f - radiusOffset);
        
        //Create a sphere of points, then do a ray cast on the object. If its a hit, create the object
        int sphereStep = System.Convert.ToInt16(Mathf.Ceil(360f/(garbageInstancesRange*10)));
        for (int i =0; i <= 360; i += sphereStep)
        {
            for(int j=0; j < 360; j += sphereStep)
            {
                float random = Random.Range(-360f, 360f);
                float theta = i * Mathf.Deg2Rad + random;
                float phi = j * Mathf.Deg2Rad + random;
                x = radius * Mathf.Sin(theta) * Mathf.Cos(phi);
                y = radius * Mathf.Sin(theta) * Mathf.Sin(phi);
                z = radius * Mathf.Cos(theta);

                Vector3 pos = (new Vector3(x, y, z) + garbagePlacementObject.transform.position);
                //GameObject go = GameObject.Instantiate(garbageObjects[0]);
                //go.transform.position = pos;

                Ray r = new Ray(pos, garbagePlacementObject.transform.position - pos);
                RaycastHit m_hit;
                Physics.queriesHitBackfaces = false;
                if (Physics.Raycast(r, out m_hit, 100f))
                {
                    if (Vector3.Distance(m_hit.point, garbagePlacementObject.transform.position) > minDistanceFromCentre)
                    {
                        int randomGarbageObjectIndex = Random.Range(0, garbageObjects.Length - 1);
                        GameObject go = GameObject.Instantiate(garbageObjects[randomGarbageObjectIndex]);
                        go.transform.position = new Vector3(m_hit.point.x, m_hit.point.y, m_hit.point.z);
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
