﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGun : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (GameStatics.canCollectGarbage && GameStatics.currentObject)
            {
                GameObject.DestroyImmediate(GameStatics.currentObject);
                GameStatics.canCollectGarbage = false;
                GameStatics.currentObject = null;
                GameStatics.count++;
            }
        }
    }
}
