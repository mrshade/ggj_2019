﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionScript : MonoBehaviour
{
    Material m_material;
    // Start is called before the first frame update
    void Start()
    {
        m_material = gameObject.GetComponent<Renderer>().material;
        m_material.color = Color.red;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!m_material)
        {
            return;
        }
        //check if there is already an object in selection, if no object is selected then select
        if(!GameStatics.canCollectGarbage)
        {
            GameStatics.canCollectGarbage = true;
            m_material.color = Color.green;
            GameStatics.currentObject = gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!m_material)
        {
            return;
        }
        if (GameStatics.canCollectGarbage)
        {
            GameStatics.canCollectGarbage = false;
            m_material.color = Color.red;
            GameStatics.currentObject = null;
        }
    }
}
